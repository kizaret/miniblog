<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>miniblog</title>
    <?php include('./includes/debug_inc.php'); ?>
    <?php include('./includes/conf_inc.php'); ?>
    <?php include('./includes/pdo_inc.php'); ?>
</head>
<body>
    <main>
        <h2>Add an article</h2>
        <form id="form_post" method="post" action="post_add_insert.php">
            <table border="1">
                <tr>
                    <th>
                        <label for="title">Title</label>
                    </th>
                    <td>
                        <input name="title" type="text"> </td>
                </tr>
                <tr>
                    <th>
                        <label for="category">Category</label>
                    </th>
                    <td>
                        <select name="category">    
                            <?php
                                try {
                                        $req = $pdo->query("SELECT * FROM categories ORDER BY cat_descr ASC");
                                        $req->setFetchMode(PDO::FETCH_ASSOC);
                                        $data = $req->fetchAll();
                                ?>
                            
                                    <?php foreach ($data as $onedata) {?>
                                        <option value="<?= $onedata["cat_id"] ?>">
                                            <?= $onedata["cat_descr"] ?>
                                        </option>
                                        <?php } ?>
                               
                                <?php
                                    $req->closeCursor();
                                }
                            catch ( Exception $e ) {
                                die("Erreur MySQL : ".$e->getMessage());
                            }
                            ?>
                        </select>
                    </td>
                </tr>
                <tr>
                    <th>
                        <label for="content">Content</label>
                    </th>
                    <td>
                        <textarea name="content" rows="10" cols="100"></textarea>
                    </td>
                </tr>
                <tr>
                    <th></th>
                    <td>
                        <input type="submit" value="enregistrer" />
                        <input type="reset" type="Effacer" /> </td>
                </tr>
            </table>
        </form>
    </main>

</body>
</html>