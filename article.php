<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>miniblog</title>
    <?php include('./includes/debug_inc.php'); ?>
	<?php include('./includes/conf_inc.php'); ?>
	<?php include('./includes/pdo_inc.php'); ?>
</head>
<body>
	<main>
		<?php include('./includes/aside.php');

			echo "<h1>Article Page</h1>";

			// Select Data
			try {
				$req = $pdo->query("SELECT * FROM posts, categories
					WHERE post_category=cat_id
				    AND post_ID =" . $_GET["id"]);

				// Send query
				$req->setFetchMode(PDO::FETCH_ASSOC);

				$data = $req->fetchAll();

				// Display Data
				foreach($data as $onedata){
					echo "<article>";
					echo "<h2><a href='article.php?id=". $onedata["post_ID"]. "'>". $onedata["post_title"]. "</a></h2>";
					echo "<p>Published at ". $onedata["post_date"]. "</p>";
					echo "<p>Category : <a href='index.php?cat=" .$onedata["cat_id"] ."'>" .$onedata["cat_descr"]."</a></p> ";
					echo "<p>". $onedata["post_content"]. "...</p>";
					echo "</article>";
				}

				// End query with cursor
				$req->closeCursor();
			}
			// Catch to prevent SQL errors
			catch(Exception $e) {
				die("Erreur SQL : ".$e->getMessage());
			}
		?>
	</main>
</body>
</html>