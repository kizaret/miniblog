<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>miniblog</title>
    <?php include('./includes/debug_inc.php'); ?>
	<?php include('./includes/conf_inc.php'); ?>
	<?php include('./includes/pdo_inc.php'); ?>
</head>
<body>
	<main>
		<h1>Miniblog - remake partiel php</h1>
		<?php include('./includes/aside.php');
			// Select Data 
			try{
				if (!isset($_GET["cat"])) {
					$query = "SELECT post_ID, post_title, post_date, post_content, post_category, cat_descr, cat_id 
					FROM posts, categories
					WHERE post_category=cat_id
					ORDER BY post_date DESC";
				}
				else {
					$query = "SELECT post_ID, post_title, post_date, post_content, post_category, cat_descr, cat_id 
					FROM posts, categories 
					WHERE post_category=cat_id 
					AND post_category=" . $_GET["cat"] . " 
					 ORDER BY post_date DESC";
				}

				// Send query
				$req = $pdo->query($query);

				$req->setFetchMode(PDO::FETCH_ASSOC);

				$data = $req->fetchAll();

				// Display Data
				foreach($data as $onedata){
					echo "<article>";
					echo "<h2><a href='article.php?id=". $onedata["post_ID"]. "'>". $onedata["post_title"]. "</a></h2>";
					echo "<p>Published at ". $onedata["post_date"]. "</p>";
					echo "<p>Category : <a href='index.php?cat=" .$onedata["cat_id"] ."'>" .$onedata["cat_descr"]."</a></p> ";
					echo "<p>". $onedata["post_content"]. "</p>";
					echo "</article>";
				}
				// End query with cursor
				$req->closeCursor();
			}
			// Catch to prevent SQL errors
			catch(Exception $e) {
				die("Erreur SQL : ".$e->getMessage());
			}
		?>
	</main>
	<footer>
		<ul>
			<li><a href="post_add.php">Add an article</a></li>
			<li><a href="posts.php">Modify / Delete Articles</a></li>
		</ul>
	</footer>
</body>
</html>