<?php
	// Connection to the host
	try
	{
		$dns = 'mysql:host=localhost;dbname=' . 'miniblog';
		
		// Connection Options
		$options = array 
		( 
			PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8",
			PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION
		);
		$pdo = new PDO($dns, 'root', 'root', $options);
	}
	catch ( Exception $e)
	{
		die("Connexion à MySQL impossible : ". $e->getMessage());
	}