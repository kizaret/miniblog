<aside>
	<h2>Categories</h2>
	<?php
		// Select Data 
		try{
			$req = $pdo->query("SELECT * 
                                FROM categories 
								ORDER BY cat_descr ASC");

			$req->setFetchMode(PDO::FETCH_ASSOC);
			$data = $req->fetchAll();
	?>
	<ul>
	<?php
			// Display Data
			foreach($data as $onedata){
	?>
		<li>
			<a href="index.php?cat=<?= $onedata["cat_id"]?>"><?= $onedata["cat_descr"]?></a>
		</li>
	<?php
		}
	?>
		<li>
			<a href="index.php">SEE ALL</a>
		</li>
	</ul>
	<?php
			//End query with cursor
			$req->closeCursor();
		}
		// Catch to prevent SQL errors
		catch(Exception $e) {
			die("Erreur SQL : ".$e->getMessage());
		}
	?>
</aside>